﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Finish controller.
/// </summary>
public class FinishController : MonoBehaviour {

	private GameObject _firstFinisher;

	public GameController gameController;

	// Use this for initialization
	void Start() {

	}
	
	// Update is called once per frame
	void Update() {
	
	}

	void OnTriggerEnter(Collider collider) {
		GameObject other = collider.gameObject;

		if(other.CompareTag("NPC")) {
			other.SendMessage("StopRace");
		}

		// TODO: Implement me!
	}
}
