﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Menu controller. Menangani tampilan menu dan interaksi tombol-tombol pada menu.
/// </summary>
public class MenuController : MonoBehaviour {

	public enum Page { Main, ChooseLevel };

	private const string DEFAULT_TITLE = "Kakak Seniorku Sangat Iseng Ketika Diajak Kenalan!";
	private const string DEFAULT_PLAY = "Main";
	private const string DEFAULT_EXIT = "Nggak Jadi Main";
	private const string DEFAULT_EASY = "Gampang Cuy";
	private const string DEFAULT_NORMAL = "Biasa Aja";
	private const string DEFAULT_HARD = "Aje Gile";
	private const string DEFAULT_BACK = "Balik";

	private float _topBannerWidth;
	private float _topBannerHeight;

	private float _buttonWidth;
	private float _buttonHeight;

	private float _buttonPos1;
	private float _buttonPos2;
	private float _buttonPos3;
	private float _buttonPos4;

	private Page _currentPage;

	public string title;

	public GUISkin titleSkin;
	public GUISkin buttonSkin;

	// Use this for initialization
	void Start() {
		InitDimensions();

		title = (title == "") ? DEFAULT_TITLE : title;

		CurrentPage = Page.Main;
	}
	
	// Update is called once per frame
	void Update() {
	
	}

	void OnGUI() {
		// Title Banner
		GUI.skin = titleSkin;
		GUI.Box(new Rect(0, 0, _topBannerWidth, _topBannerHeight), title);

		// Tampilkan tombol-tombol sesuai dengan halaman menu
		GUI.skin = buttonSkin;
		if(CurrentPage == Page.Main)
			ShowMainMenu();
		else if(CurrentPage == Page.ChooseLevel)
			ShowChooseLevel();
	}

	public Page CurrentPage {
		get { return _currentPage; }
		set { _currentPage = value; }
	}

	
	private void InitDimensions() {
		_topBannerWidth = Screen.width;
		_topBannerHeight = Screen.height / 5;
		
		_buttonWidth = Screen.width;
		_buttonHeight = Screen.height / 8;

		_buttonPos1 = _topBannerHeight;
		_buttonPos2 = _buttonPos1 + _buttonHeight;
		_buttonPos3 = _buttonPos2 + _buttonHeight;
		_buttonPos4 = _buttonPos3 + _buttonHeight;
	}

	private void ShowMainMenu() {
		// Play Button + event handler
		if(GUI.Button(new Rect(0, _buttonPos1, _buttonWidth, _buttonHeight), DEFAULT_PLAY)) {
			CurrentPage = Page.ChooseLevel;
		}

		// Exit Button + event handler
		if(GUI.Button(new Rect(0, _buttonPos2, _buttonWidth, _buttonHeight), DEFAULT_EXIT)) {
			Application.Quit();
		}
	}

	private void ShowChooseLevel() {
		// Easy Button + event handler
		if(GUI.Button(new Rect(0, _buttonPos1, _buttonWidth, _buttonHeight), DEFAULT_EASY)) {
			// TODO: Implement me!
		}

		// Normal Button + event handler
		if(GUI.Button(new Rect(0, _buttonPos2, _buttonWidth, _buttonHeight), DEFAULT_NORMAL)) {
			PlayerPrefs.SetString("Difficulty", "Normal");
			PlayerPrefs.Save();
			DefaultPlay();
		}

		// Hard Button + event handler
		if(GUI.Button(new Rect(0, _buttonPos3, _buttonWidth, _buttonHeight), DEFAULT_HARD)) {
			// TODO: Implement me!
		}

		// Back Button + event handler
		if(GUI.Button(new Rect(0, _buttonPos4, _buttonWidth, _buttonHeight), DEFAULT_BACK)) {
			CurrentPage = Page.Main;
		}
	}

	private void DefaultPlay() {
		Application.LoadLevel(1);
	}
}
