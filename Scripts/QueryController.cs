﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(NavMeshAgent))]
[RequireComponent(typeof(QueryMechanimController))]

/// <summary>
/// Query controller.
/// </summary>
public class QueryController : MonoBehaviour {

	// State permainan karakter Query
	public enum PlayState { Countdown, Race, Finish };
	
	private NavMeshAgent _agent;
	private QueryMechanimController _animController;

	private float _originalSpeed;
	private PlayState _state;

	public GameObject destination;

	// Use this for initialization
	void Start() {
		_agent = GetComponent<NavMeshAgent>();
		_animController = GetComponent<QueryMechanimController>();

		_originalSpeed = _agent.speed;
		_state = PlayState.Countdown;
	}
	
	// Update is called once per frame
	void Update() {

	}

	void FixedUpdate() {
		// Atur animasi sesuai dengan state permainan karakter
		if(_state == PlayState.Race)
			_animController.ChangeAnimation(QueryMechanimController.QueryChanAnimationType.RUN);

		if(_state == PlayState.Countdown)
			_animController.ChangeAnimation(QueryMechanimController.QueryChanAnimationType.IDLE);

		if(_state == PlayState.Finish)
			_animController.ChangeAnimation(QueryMechanimController.QueryChanAnimationType.IDLE);
	}
	
	public void RevertSpeed() {
		_agent.speed = _originalSpeed;
	}

	public void StartRace() {
		_state = PlayState.Race;
		_agent.SetDestination(destination.transform.position);
	}

	public void StopRace() {
		_state = PlayState.Finish;
		_agent.Stop();
	}
}
