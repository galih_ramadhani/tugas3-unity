﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Game controller.
/// </summary>
public class GameController : MonoBehaviour {

	private float _centerX;
	private float _centerY;

	public float counter;
	public GameObject player;
	public GameObject npc;

	public GUISkin countdownSkin;

	// Use this for initialization
	void Start () {
		// Posisi tengah layar untuk lokasi tampilan hitung mundur
		_centerX = Screen.width / 2;
		_centerY = Screen.height / 2;

		// Jalankan hitung mundur
		StartCoroutine(Countdown());

		// Hint: mungkin disini kalian bisa ambil pilihan difficulty dari pemain yang
		// tersimpan di PlayerPrefs
	}
	
	// Update is called once per frame
	void Update() {
	
	}

	void OnGUI() {
		// Tampilkan hitung mundur
		if(counter >= 0)
			ShowCountdown();
	}

	private void ShowCountdown() {
		GUI.skin = countdownSkin;
		GUI.Box(new Rect(_centerX, _centerY, Screen.width / 10, Screen.height / 10), 
		        ((int) counter + 1).ToString("D"));
		GUI.skin = null;
	}

	private IEnumerator Countdown() {
		// Hitung mundur hingga 0
		while(counter >= 0) {
			yield return 0;
			counter = counter - Time.deltaTime;
		}

		// Mulai lomba dengan memanggil method StartRace() di masing-masing karakter
		player.SendMessage("StartRace");
		npc.SendMessage("StartRace");
	}
}
